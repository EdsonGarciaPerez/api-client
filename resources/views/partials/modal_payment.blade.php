<div id="modal-payment" class="modal bottom-sheet">
    <div class="modal-content">
        <div class="container">
            <div class="row" id="loader">
                <div class="col s12 d-flex justify-content-center">
                    <div class="preloader-wrapper big active">
                        <div class="spinner-layer spinner-teal-only">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div><div class="gap-patch">
                                <div class="circle"></div>
                            </div><div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col s12 text-center">
                    <h5>@lang('dictionary.main.load')</h5>
                </div>
            </div>
            <div class="row hidden" id="message">
                <div class="col s12 text-center">
                    <h5>@lang('dictionary.main.success')</h5>
                    <i class="material-icons green-text">check_circle</i><br>
                    <p>@lang('dictionary.main.message')</p>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</div>