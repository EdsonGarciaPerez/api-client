@extends('layouts.index')

{{-- section for load page name --}}
@section('name') Inicio @endsection

{{-- Section for load external Css files --}}
@section('css')@endsection

{{-- main section --}}
@section('main')
    <div class="timeline grey lighten-4">
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <p>
                        <a href="#!">@lang('dictionary.navbar.home')</a> / <a class="grey-text darken-4" href="">@lang('dictionary.main.products')</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col s12 m4">
                <div class="row">
                    <div class="col s12">
                        <h4>@lang('dictionary.main.products') ( {{ count($products) }} ) </h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col s12">
                        <div class="card no-margin-t car">
                            <div class="card-content grey lighten-4">
                                <h5>@lang('dictionary.main.order'): <span class="number-order" data-order="{{ $order->number }}">{{ $order->number }}</span></h5>
                            </div>
                            <div class="card-content no-padding">
                                <ul id="items-order" class="collection no-margin">
                                    @foreach ($order->items as $item)
                                        <li class="collection-item item-order">
                                            <a class="btn-floating btn-delete halfway-fab waves-effect waves-light red tooltipped"
                                            data-position="top" data-tooltip="@lang('dictionary.main.delete')"><i class="material-icons">delete</i></a>
                                            <div class="row no-margin-b d-flex align-center">
                                                <div class="col s3">
                                                    <img src="{{ $item->imageUrl }}" width="100%">
                                                </div>
                                                <div class="col s6">
                                                    <span>{{ $item->name }}</span><br/>
                                                    <small class="sku" data-sku="{{ $item->sku }}">sku:{{ $item->sku }}</small><br/>
                                                    <span class="quantity" data-quantity="{{ $item->pivot->quantity }}"><b>{{ $item->pivot->quantity }} pza</b></span>
                                                </div>
                                                <div class="col s3">
                                                    <span class="price teal-text darken-4" data-price="{{ $item->pivot->price }}"><b>$ {{ $item->pivot->price * $item->pivot->quantity }} {{ $order->currency }}</b></span>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="card-action text-center">
                                <button type="button" id="btn-payment" class="waves-effect waves-light btn text-center modal-trigger" href="#modal-payment"><i class="material-icons left">payment</i>@lang('dictionary.main.payment')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col s12 m8">
                <div class="row no-margin-b">
                    <div class="input-field col s12 m6">
                        <i class="material-icons prefix">search</i>
                        <input id="searcher" type="text">
                        <label for="searcher">@lang('dictionary.main.search') ...</label>
                    </div>
                </div>
                <div class="row">
                    @foreach($products as $key => $product)
                        <div class="col s12 m4 product">
                            <div class="card">
                                <div class="card-image">
                                    <img src="{{ $product->imageUrl }}">
                                    <button type="button" data-name="{{ $product->name }}" data-sku="{{ $product->sku }}" data-currency="{{ $order->currency }}"
                                        data-image="{{ $product->imageUrl }}" data-description="{{ $product->description }}"  data-price="{{ $product->price }}"
                                        class="add-product tooltipped btn-floating halfway-fab waves-effect waves-light red"
                                        data-position="top" data-tooltip="@lang('dictionary.main.add')"><i class="material-icons">add_shopping_cart</i></button>
                                </div>
                                <div class="card-content">
                                    <span class="card-title">{{ $product->name }}</span>
                                    <p>{{ $product->description }}</p>
                                </div>
                                <div class="card-action">
                                    <span>SKU:{{ $product->sku }}</span>
                                    <span class="right teal-text darken-4">$ {{ $product->price }} MXN</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('modals')
    @include('partials.modal_payment')
@endsection

{{-- section for load externa js files --}}
@section('js')@endsection