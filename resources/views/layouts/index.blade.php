<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ (env('APP_NAME')) ? env('APP_NAME') : 'Proyecto Front'  }} - @yield('name')</title>

        {{------------------------------------------------------------------------------------
        --- Fonts
        -------------------------------------------------------------------------------------}}
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        {{------------------------------------------------------------------------------------
        --- External CSS CDN's 
        -------------------------------------------------------------------------------------}}
        {{-- MaterializeCss --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
        {{-- MaterializeCss Icons --}}
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        {{------------------------------------------------------------------------------------
        --- Local CSS
        -------------------------------------------------------------------------------------}}
        {{-- Flex Style CSS --}}
        <link rel="stylesheet" href="{{ asset('css/flex.css') }}">
        {{-- Default Style CSS --}}
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        @yield('css')
        
    </head>
    <body>
        {{-- Navbar site --}}
        <nav id="navbar" class="white text-black">
            <div class="nav-wrapper container">
                <a href="#" class="brand-logo"><img src="{{ asset('images/logo.png') }}" alt="@lang('dictionary.navbar.brand')"></a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a class="teal-text darken-4" href="#!">@lang('dictionary.navbar.home')</a></li>
                    <li><a class="teal-text darken-4" href="#!">@lang('dictionary.navbar.products')</a></li>
                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                        @unless (LaravelLocalization::getCurrentLocale() == $localeCode)
                            <li>
                                <a class="teal-text darken-4" rel="alternate" style="text-transform: capitalize;" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                    <img src="{{ asset('images/').'/'.$localeCode.'.png' }}" width="20px"> {{ $properties['native'] }}    
                                </a>
                            </li>
                        @endunless
                    @endforeach
                </ul>
            </div>
        </nav>

        {{-- Main container --}}
        <main>
            @yield('main')
        </main>

        {{-- Footer container --}}
        <footer class="page-footer white">
            <div class="footer-copyright">
                <div class="container text-center teal-text darken-4">
                    © {{ Carbon\Carbon::now('America/Mexico_City')->format('Y') }} @lang('dictionary.footer.copyright')
                </div>
            </div>
        </footer>
        {{------------------------------------------------------------------------------------
        --- External JS CDN's
        -------------------------------------------------------------------------------------}}
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
        {{------------------------------------------------------------------------------------
        --- Local JS
        -------------------------------------------------------------------------------------}}
        <script src="{{ asset('js/app.js') }}"></script>
        @yield('js')
        {{------------------------------------------------------------------------------------
        --- Modals
        -------------------------------------------------------------------------------------}}
        @yield('modals')
    </body>
</html>
