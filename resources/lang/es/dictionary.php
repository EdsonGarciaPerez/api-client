<?php

return [

    'navbar' => [
        'brand' => 'Logo',
        'home' => 'Inicio',
        'products' => 'Productos',
    ],
    'main' => [
        'search' => 'Buscar',
        'products' => 'Productos',
        'order' => 'Orden',
        'payment' => 'Realizar Pago',
        'add' => 'Añadir a la Orden',
        'delete' => 'Eliminar de la Orden',
        'load' => 'Procesando pago',
        'success' => 'Envío Exitoso',
        'message' => 'El proceso de pago finalizo con exito.',
    ],
    'footer' => [
        'copyright' => 'Todos los derechos reservados.',
    ],
];
