<?php

return [

    'navbar' => [
        'brand' => 'Brand',
        'home' => 'Home',
        'products' => 'Products',
    ],
    'main' => [
        'search' => 'Search',
        'products' => 'Products',
        'order' => 'Order',
        'payment' => 'Make Payment',
        'add' => 'Add to Order',
        'delete' => 'Delete to Order',
        'load' => 'Processing payment',
        'success' => 'Successful Shipping',
        'message' => 'The payment process ended successfully.',
    ],
    'footer' => [
        'copyright' => 'All rights reserved.',
    ],
];
