<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class LandingController extends Controller
{
    public function welcome(){
        $auth = [
            'email' => env('API_USERNAME'),        
            'password' => env('API_PASSWORD'),        
        ];

        $headers = [
            "Content-Type" => 'application/json',
        ];

        $client = new Client(['base_uri' => 'http://localhost:8000/api/']);
        
        $response = $client->request('POST', 'iniciar-sesion', [
            'headers' => $headers,
            'body' => json_encode($auth)
        ]);

        if ($response->getStatusCode() == 200){
            $token = json_decode($response->getBody()->getContents());
        }
        
        $headers = [
            "Content-Type" => 'application/json',
            'Authorization' => $token->token_type." ".$token->access_token,        
        ];

        $products_request = $client->request('GET', 'products', [
            'headers' => $headers
        ]);
        if ($products_request->getStatusCode() == 200){
            $products = json_decode($products_request->getBody()->getContents());
        }

        $order_request = $client->request('GET', 'order/2291137839244', [
            'headers' => $headers
        ]);
        if ($order_request->getStatusCode() == 200){
            $order = json_decode($order_request->getBody()->getContents());
        }
        // dd($order, $products);
        return view('landing.welcome', compact('products', 'order'));
    }
}
