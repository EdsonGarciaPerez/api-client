$(function(){
    // Initialize MaterializeCss
    $('.tooltipped').tooltip();
    var modal = $('#modal-payment').modal({
        dismissible: false,
        onOpenEnd: function() { // Callback for Modal open. Modal and trigger parameters available.
            setTimeout(() => {
                $("#modal-payment").find("#loader").fadeOut("slow", function(){
                    $("#modal-payment").find("#message").fadeIn("slow");
                });
                closeModal();
            }, 4000);
        },
        onCloseEnd: function() { // Callback for Modal open. Modal and trigger parameters available.
            $("#modal-payment").find("#message").fadeOut("fast", function(){
                $("#modal-payment").find("#loader").fadeIn("fast");
            });
        },
    });
    function closeModal(){
        setTimeout(() => {
            $(".item-order").each(function(e) {
                $(this).fadeOut("slow", function(){
                    $(this).remove();
                });
            });
            order = $('.number-order').data("order");
            order++;
            $('.number-order').data('order', order);
            $('.number-order').html(order);
            $("#btn-payment").attr("disabled", true);
            modal.modal("close");
        }, 4000);
    }
    // Function to add products to the order
    $(document).on("click", ".add-product", function(){
        var name = $(this).data("name"),
            price = $(this).data("price"),
            currency = $(this).data("currency"),
            image = $(this).data("image"),
            sku = $(this).data("sku"),
            item_exist = false,
            item = {},
            item_sku;
        // Validate if the item exist on the order
        $(".item-order").each(function(e) {
            item_sku = $(this).find(".sku").data('sku');
            if(item_sku == sku){
                item_exist = true;
                item = $(this);
            }
        });
        if(item_exist){
            // if item exist in the order, update de quantity
            quantity = item.find('.quantity').data("quantity");
            price = item.find('.price').data("price");
            quantity++;
            item.find('.quantity').data('quantity', quantity);
            item.find('.quantity').html('<b>'+quantity+' pza</b>');
            item.find('.price').html('<b>$ '+price * quantity+' '+currency+'</b>');
        }else{
            // if the item doesn't exist, add the product to the order
            $("#items-order").append('<li class="collection-item item-order">\
                <a class="btn-floating btn-delete halfway-fab waves-effect waves-light red tooltipped"\
                data-position="top" data-tooltip="Eliminar de la Orden"><i class="material-icons">delete</i></a>\
                <div class="row no-margin-b d-flex align-center">\
                    <div class="col s3">\
                        <img src="'+image+'" width="100%">\
                    </div>\
                    <div class="col s6">\
                        <span>'+name+'</span><br/>\
                        <small class="sku" data-sku="'+sku+'">sku:'+sku+'</small><br/>\
                        <span class="quantity" data-quantity="1"><b>1 pza</b></span>\
                    </div>\
                    <div class="col s3">\
                        <span class="price teal-text darken-4" data-price="'+price+'"><b>$ '+price+' '+currency+'</b></span>\
                    </div>\
                </div>\
            </li>');
            $('.tooltipped').tooltip();    
            $("#btn-payment").removeAttr("disabled");
        }
    });

    // Function for delete products of the order
    $(document).on("click", ".btn-delete", function(){
        $(this).parent().fadeOut("slow", function(){
            $(this).remove();
            if($(".item-order").length == 0){
                $("#btn-payment").attr("disabled", true);
                $('.material-tooltip').css("visibility", "hidden"); 
            }
        });
    });

    //Function for filter de products
    $(document).on("keyup", "#searcher", function(){
        if(($(this).val()).length >= 3){
            filter_text = $(this).val();
            $(".product").each(function(e) {
                text = $(this).find(".card-title").html().toLowerCase();
                console.log(filter_text);
                if(text.search(filter_text.toLowerCase()) < 0){
                    $(this).fadeOut("slow");
                }else{
                    $(this).fadeIn("slow");
                }
            });
        }else{
            $(".product").each(function(e) {
                $(this).fadeIn("slow");
            });

        }
    });


    
});